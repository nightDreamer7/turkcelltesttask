//
//  CurrentProduct.swift
//  TurkcellTestTask
//
//  Created by Alex on 8/22/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import Foundation

// MARK: - CurrentProduct
struct CurrentProduct: Codable {
    let productID, name: String
    let price: Int
    let image: String
    let currentProductDescription: String
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case name, price, image
        case currentProductDescription = "description"
    }
}

//
//  ItemModel.swift
//  TurkcellTestTask
//
//  Created by Alex on 8/22/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import Foundation

// MARK: - Item
struct Item: Codable {
    let products: [Product]
}

// MARK: - Product
struct Product: Codable {
    let productID, name: String
    let price: Int
    let image: String
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case name, price, image
    }
}


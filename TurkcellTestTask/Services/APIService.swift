//
//  APIService.swift
//  TurkcellTestTask
//
//  Created by Alex on 8/22/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import Foundation

final class APIService {
    
    // MARK: Shared Instance
    static let shared = APIService()

    // MARK: - Public API
    var products: [Product] = []
    
    func fetchData() {
        let urlString = "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/list"
        guard let url = URL(string: urlString) else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
            } else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let jsonData = data,  let item = try? JSONDecoder().decode(Item.self, from: jsonData) {
                    item.products.forEach({ (product) in
                        self.products.append(product)
                    })
                }
            }
        }
        task.resume()
    }
}

//
//  PreviewCollectionViewController.swift
//  TurkcellTestTask
//
//  Created by Alex on 8/22/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import UIKit

    class PreviewCollectionViewController: UIViewController {
        
        // Private API
        private var products: [Product] {
            return APIService.shared.products
        }
        
        // MARK: - Outlets
        @IBOutlet weak var collectionView: UICollectionView!
        
        override func viewDidLoad() {
            super.viewDidLoad()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            collectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
                self.collectionView.reloadSections(IndexSet(integer: 0))
                }
            }
        }
    
    // MARK: - UICollectionViewDataSource
    extension PreviewCollectionViewController: UICollectionViewDataSource {
        // Cell for collection view at:
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            // Optional value chaining, if we have any values func catches it and returns cell with it, by default, it returns empty cell
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell {
                let index = indexPath.row
                cell.populateWith(item: products[index])
                return cell
            }
            
            return UICollectionViewCell()
        }
        
        // Number of collection view items
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return products.count - 1
        }
    }
    // MARK: - UICollectionViewDelegate
    extension PreviewCollectionViewController: UICollectionViewDelegate {

        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let cellIndex = indexPath.row
            let selectedProduct = products[cellIndex]
            let sb = UIStoryboard(name: "Main", bundle: nil)
            guard let vc = sb.instantiateViewController(withIdentifier: "ViewController") as? ViewController else { return }
            vc.poductID = selectedProduct.productID
                self.navigationController?.pushViewController(vc, animated: true)
        }
}

extension PreviewCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionView.frame.width / 2.08, height: collectionView.frame.height / 3.2)
    }
}

//
//  ViewController.swift
//  TurkcellTestTask
//
//  Created by Alex on 8/22/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    
    // MARK: - Public API
    var poductID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchDescription()
    }
    
    // MARK: - Private API
    private func fetchDescription() {
        guard let url = URL(string: "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/\(poductID)/detail") else { return }
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let currentProduct = try? JSONDecoder().decode(CurrentProduct.self, from: data)
            self.nameLabel.text = currentProduct?.name
            guard let priceString = currentProduct?.price else { return }
            self.priceLabel.text = "\(priceString)"
            self.productDescriptionLabel.text = currentProduct?.currentProductDescription
            guard let imageUrl = currentProduct?.image else { return }
            self.productImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder.png"))
        }
        task.resume()
    }

    // MARK: - Actions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


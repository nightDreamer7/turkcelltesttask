//
//  ProductCollectionViewCell.swift
//  TurkcellTestTask
//
//  Created by Alex on 8/22/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCollectionViewCell: UICollectionViewCell {

    // MARK: - Outlets
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    // MARK: - Public API
    func populateWith(item: Product) {
       itemNameLabel.text = item.name
       productPriceLabel.text = String(item.price)
       let imageUrl = item.image
       itemImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder.png"))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.borderWidth = 5
        self.itemImage.layer.borderWidth = 3
        self.itemImage.layer.borderColor = UIColor.darkGray.cgColor
    }

}
